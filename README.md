![coverage](https://gitlab.com/b00leant/woofer/-/badges/release.svg)

# Woofer

Woofer is a project created for the job application at Deinde

## Getting Started

Woofer project is a mobile implementation of the `dogs.ceo` REST API.
The whole set of web services offers textual informations, basically name and url, of each dog breed in the world.

This app used the following REST API:
- https://dog.ceo/api/breed/hound/images/random
- https://dog.ceo/api/breeds/image/random
- https://dog.ceo/api/breed/hound/images
- https://dog.ceo/api/breeds/list/all
- https://dog.ceo/api/breed/hound/afghan/images/random
- https://dog.ceo/api/breed/hound/afghan/images

where "hound" and "afghan" are respectively the two custom path params
- `$breed`
- `$subBreed`

Woofer enhanced this simple requests with a UI/UX design that makes easier to naigate inside each breed/sub-breed.
In order to have a much better and less lagged navigation, I decided to run one big data loading at the very first beginning of the app, mantaining the breed dataset in persistance using the [SharedPreferences](https://pub.dev/packages/shared_preferences) library to manage the Storage in a `Map<string,string>` structure.

## How to Use

**Step 1:**

clone this repo by using the command below:

```
git clone https://gitlab.com/b00leant/woofer.git
```

**Step 2:**

Go to project root and execute the following command in console to get the required dependencies:

```
flutter pub get 
```

## Test
To run tests, simply hit the following command
``````
flutter test --coverage
``````
or simply run `flutter test test/$any_single_test_file.dart`

## Make some documentation

To generate html dart doc, use the following commands (a env variable called `FLUTTER_ROOT` is required)
``````
dart doc . 
``````

``````
# generate `coverage/lcov.info` file
dart doc . 
``````
If you have [genhtml](https://github.com/linux-test-project/lcov/blob/master/bin/genhtml)
you can generate HTML reports based on this .info output

Note: on macOS you need to have lcov installed on your system (`brew install lcov`) to use this:
```
genhtml coverage/lcov.info -o coverage/html
# now open the report with
open coverage/html/index.html
```

## Woofer Features:

* Breed List
* Breed Detail (Breed and Sub Breed)
* GridList
* Random Photo Shuffle (used inside the `DetailView`'s avatar)

## Extra Features

* Image URL Copy (on grid item long press)
* Dialog (on grid item tap)
* Data Persistence
* Dark/Light theme switch
* AutoScroll on top
* Breed Search

### Up-Coming Features:

* Image Cache 💾
* Internet Connectivity Check 🛜

### Used Flutter packages 

* [network_image_mock](https://pub.dev/packages/network_image_mock)
* [theme_provider](https://pub.dev/packages/theme_provider)
* [shared_preferences](https://pub.dev/packages/shared_preferences)
* [http](https://pub.dev/packages/http)
* [mockito](https://pub.dev/packages/mockito)

### Folder Structure
Here is the core folder structure which flutter provides.

```
flutter-app/
|- android
|- build
|- ios
|- lib
|- test
```

Here is the folder structure we have been using in this project

```
lib/
|- constants/
│  ├─ routes.dart
│  ├─ themes.dart
|- delegates/
│  ├─ search_delegate.dart
|- widgets/
│  ├─ components/
│  │  ├─ dialog_content.dart
│  │  ├─ dynamic_image_avatar.dart
│  │  ├─ grid_item.dart
│  │  ├─ image_avatar.dart.dart
│  │  ├─ list_item.dart
│  ├─ views/
│  │  ├─ base_layout_view.dart
│  │  ├─ detail_view.dart
│  │  ├─ grid_list_view.dart
│  │  ├─ main_list_view.dart
|- models/
│  ├─ basic_response.dart
│  ├─ breed.dart
│  ├─ pic_item.dart
|- utils/
│  ├─ helpers.dart
|- services/
│  ├─ api_services.dart
|- main.dart
```

Now, lets dive into the lib folder which has the main code for the application.

```
1- constants - All the application level constants are defined in this directory with-in their respective files. This directory contains the constants for Material `themes`, `routes`, `endpoints`.
2- delegates - Contains the delegates used in views.
3- widgets - Contains the common widgets, subdivided into components and views
4- models - Contains the data layer of your project, includes directories for local, network and shared pref/cache.
5- utils - Contains the helper methods and utils written to simplify the coding part
6- services - Contains the external services used in the app logic, for example REST API integrations
7- main.dart - This is the starting point of the application. All the application level configurations are defined in this file i.e, theme, routes, title, orientation etc.
```

## Wiki

Checkout [wiki](https://gitlab.com/b00leant/woofer/-/wikis/Woofer-Wiki) for more info

## Conclusion

I hope to have made it clear, describing as much as I can the behaviour, architecture and the design of this app.
If you have some Q&A don't exhitate to open an issue, I'll be there for you 🦆

---> For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
