
/// *
/// it's a best practice to place [apiBaseURL] (like any API url)
/// from the hardcode style to a correct configuration
const String apiBaseURL = "https://dog.ceo/api";