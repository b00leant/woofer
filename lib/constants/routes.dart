import 'package:flutter/cupertino.dart';
import 'package:woofer/widgets/views/detail_view.dart';
import 'package:woofer/widgets/views/main_list_view.dart';

///
/// constant that defines the routes of Woofer app
final Map<String,WidgetBuilder> routes = <String, WidgetBuilder>
{
  '/list': (context) => const MainListView(),
  '/detail': (context) => DetailView(),
};

