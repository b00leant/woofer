import 'package:flutter/material.dart';
import 'package:theme_provider/theme_provider.dart';

/// function that returns the list of the app AppTheme objects
///
/// edit [themes] if you want to add more themes or change the
/// dark/light alternatives
List<AppTheme> getAppThemes(BuildContext context){
  List<AppTheme> themes = [
    AppTheme(
        description: "lightTheme",
        id:"light",
        data: ThemeData(
          brightness: Brightness.light,
          primaryColor: Theme
              .of(context)
              .colorScheme
              .primary,
          secondaryHeaderColor: Colors.white,
          fontFamily: 'Arial',
          colorScheme: ColorScheme.fromSeed(
              seedColor: Colors.deepPurple),
          useMaterial3: true,
        )
    ),
    AppTheme(
        description: "darkTheme",
        id:"dark",
        data: ThemeData(
          brightness: Brightness.dark,
          primaryColor: Theme
              .of(context)
              .colorScheme.primary,
          secondaryHeaderColor: Colors.white,
          fontFamily: 'Arial',
          useMaterial3: true,
        )
    )
  ];
  return themes;
}