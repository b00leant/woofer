import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:theme_provider/theme_provider.dart';
import 'package:woofer/model/breed.dart';


/// helper to have better rendered texts
String capitalize(String s) => s[0].toUpperCase() + s.substring(1);

void loadShareBottomMenu(BuildContext context,String url){
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: 200,
          color: Theme.of(context).colorScheme.background,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text('Share this breed photo!',style: Theme.of(context).textTheme.bodyLarge,),
                ElevatedButton.icon(
                    icon: const Icon(Icons.copy),
                    label: const Text('Close BottomSheet'),
                    onPressed: () =>
                    {
                      Clipboard.setData(
                          ClipboardData(text: url)).then((
                          value) {
                        Navigator.pop(context);
                        ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                                backgroundColor: Theme.of(context).colorScheme.inversePrimary,
                                content: Row(children: [
                                  Icon(Icons.copy,color:  Theme.of(context).colorScheme.secondary,),
                                  Text(' URL Copied successfully!',style: Theme.of(context).textTheme.bodyMedium?.copyWith(color: Theme.of(context).colorScheme.secondary ))
                                ],)
                            ));
                      },
                      ),
                    }
                )],
            ),
          ),
        );
      },
    );
}

List<Widget> createAppBarActions(BuildContext context, [List<IconButton>? extra]) {
  var controller = ThemeProvider.controllerOf(context);
  List<Widget> ex = [];
  if(extra!=null){
    ex.addAll(extra);
  }

  return [
    IconButton(
      icon: const Icon(Icons.brightness_4),
      onPressed : controller.nextTheme,
    ),
    ...ex
    /*if(extra!.isNotEmpty)...[
      ...extra
    ] else ...[

    ]*/
  ];
}
/// helper used to decode the JSON string saved on persistence
List<Breed> breedListFromJson(String str) =>
    List<Breed>.from(json.decode(str).map((x) => Breed.fromJson(x)));

/// *
/// Get the whold List<Breed> data structure without imageURL
///
/// Add to each Breed element of [breedDataList] a random image
List<Breed> getBreedDataListFromJSON(Map<String, dynamic> json)  {
  List<Breed> breeds = [];
  json.forEach((key, value) {
    List<String> subBreedsStr = List<String>.from(json[key]);
    Breed breed = Breed(name: key, subBreeds: [], imageURL: "");
    for(String str in subBreedsStr){
      Breed subBreed = Breed(name: str, subBreeds: [],imageURL: "");
      breed.subBreeds.add(subBreed);
    }
    breeds.add(breed);
  });
  return breeds;
}

/// *
/// this function clean out the SharedPreferences for each key used by
/// this app
/// edit it if you want to remove programmatically some cached vars
void resetPersistence() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.remove('breedDataList');
}