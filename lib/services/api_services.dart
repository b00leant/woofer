import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:woofer/constants/endpoints.dart';
import 'package:woofer/model/basic_response.dart';
import 'package:woofer/model/breed.dart';
import 'package:woofer/utils/helpers.dart';


class APIService {

  final http.Client client;

  APIService(this.client);

  Map<String,String> headers = {
    'Content-Type':'application/json'
  };


  /// *
  /// function used to invoke the REST API `breed/image/random`
  /// offered my "dog.ceo" using a GET HTTP Response
  ///
  /// this function returns a string of an URL of a random breed or sub-breed
  Future<String> getRandomImageURL() async {

    var url = Uri.parse('$apiBaseURL/breed/image/random');
    final responseData = await client.get(
      url
    );
    String picURL = "";
    if (responseData.statusCode == 200 && responseData.body.isNotEmpty) {
      picURL = BasicResponse.fromJson(jsonDecode(responseData.body)).message;
    } else {
      throw Exception('Failed to load data from API');
    }
    return Future.value(picURL);
  }

  /// *
  /// function used to invoke the REST API `breed/[breed]/images/random`
  /// offered my "dog.ceo" using a GET HTTP Response
  ///
  /// this function returns a List<String> containing
  /// a set URLs of a random image by [breed]
  ///TODO manage the "limit" feature that the webservice offers
  Future<String> getSingleRandomBreedImage(String breed) async {
    var url = Uri.parse('$apiBaseURL/breed/$breed/images/random');

    final responseData = await client.get(
      url
    );

    String imageURL = "";

    if (responseData.statusCode == 200 && responseData.body.isNotEmpty) {
      imageURL = BasicResponse.fromJson(jsonDecode(responseData.body)).message;
    } else {
      throw Exception('Failed to load data from API');
    }
    return Future.value(imageURL);
  }

  /// *
  /// function used to invoke the REST API `breeds/list/all`
  /// offered my "dog.ceo" using a GET HTTP Response
  ///
  /// this function returns the whole List<Breed> data structure with no
  /// related [imageURL].
  ///
  /// All the empty urls will be filled in the data-mesh-like feature in future,
  /// combining this function with others..
  Future<List<Breed>> getBreedList() async {
    var url = Uri.parse('$apiBaseURL/breeds/list/all');
    final responseData = await client.get(url);

    List<Breed> breedDataList = [];

    if (responseData.statusCode == 200 && responseData.body.isNotEmpty) {
      BasicResponse response = BasicResponse.fromJson(
          jsonDecode(responseData.body));
      breedDataList = getBreedDataListFromJSON(response.message);

    } else {
      throw Exception('Failed to load data from API');
    }
    return breedDataList;
  }


  /// *
  /// function used to invoke the REST API `breeds/subBreed/images`
  /// offered my "dog.ceo" using a GET HTTP Response
  ///
  /// this function returns a random String of one breed
  /// url by [breed] and [subBreed]
  ///
  /// - [breedName] is the master breed
  /// - [subBreed] is the sub-breed
  Future<String> getSingleSubBreedRandomImage(String breed,String subBreed) async {

    var url = Uri.parse('$apiBaseURL/breed/$breed/$subBreed/images/random');
    final responseData = await client.get(
      url
    );

    String randomURL = "";

    if (responseData.statusCode == 200 && responseData.body.isNotEmpty) {
      BasicResponse response = BasicResponse.fromJson(jsonDecode(responseData.body));
      randomURL = response.message;

    } else {
      throw Exception('Failed to load data from API');
    }
    return Future.value(randomURL);
  }


  /// *
  /// function used to invoke the REST API `breeds/subBreed/images`
  /// offered my "dog.ceo" using a GET HTTP Response
  ///
  /// this function returns the whole List<String> of url in an
  /// array-like structure with by [breed] and [subBreed]
  ///
  /// - [breed] is the master breed
  /// - [subBreed] is the sub-breed
  Future<List<String>> getSubBreedImages(String breed,String subBreed) async {

    var url = Uri.parse('$apiBaseURL/breed/$breed/$subBreed/images');
    final responseData = await client.get(
      url
    );

    List<String> allURLS = [];

    if (responseData.statusCode == 200 && responseData.body.isNotEmpty) {
      BasicResponse response = BasicResponse.fromJson(jsonDecode(responseData.body));
      allURLS = List<String>.from(response.message);

    } else {
      throw Exception('Failed to load data from API');
    }
    return Future.value(allURLS);
  }

  ///
  /// function used to invoke the REST API `breeds/images`
  /// offered my "dog.ceo" using a GET HTTP Response
  ///
  /// this function returns the whole List<String> of url in an
  /// array-like structure with by [breed]
  ///
  /// - [breed] is the master breed
  Future<List<String>> getBreedImages(String breed) async {

    var url = Uri.parse('$apiBaseURL/breed/$breed/images');

    final responseData = await client.get(
      url
    );

    List<String> allURLS = [];

    if (responseData.statusCode == 200 && responseData.body.isNotEmpty) {
      BasicResponse response = BasicResponse.fromJson(jsonDecode(responseData.body));
      allURLS = List<String>.from(response.message);

    } else {
      throw Exception('Failed to load data from API');
    }
    return Future.value(allURLS);
  }
}

