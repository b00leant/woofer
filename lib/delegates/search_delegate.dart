import 'package:flutter/material.dart';
import 'package:woofer/model/breed.dart';
import 'package:woofer/utils/helpers.dart';
import 'package:woofer/widgets/views/detail_view.dart';

///
/// [DataSearch] class is a custom [SearchDelegate]
/// used to enhance the search purposes inside Woofer
///
/// - [breedList] is a [List] containing all the suggestions
/// that will be showed, at each tap corresponds a navigation
/// through [DetailView]
class DataSearch extends SearchDelegate<String> {

  final List<Breed> breedList;

  DataSearch(this.breedList);

  @override
  List<Widget> buildActions(BuildContext context) {
    return [IconButton(icon: const Icon(Icons.clear), onPressed: () {
      query = '';
    })];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
        icon: AnimatedIcon(icon: AnimatedIcons.menu_arrow,
          progress: transitionAnimation,
        ),
        onPressed: () {
          close(context, "");
        });
  }

  @override
  Widget buildResults(BuildContext context) {
    final suggestionList = breedList;
    return ListView.builder(itemBuilder: (context, index) => ListTile(

      title: Text(breedList[index].name),
    ),
      itemCount: suggestionList.length,
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final suggestionList = query.isEmpty
        ? breedList
        : breedList.where((p) => p.name.contains(RegExp(query, caseSensitive: false))).toList();
    return ListView.builder(itemBuilder: (context, index) => ListTile(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) =>
                DetailView(breedData: suggestionList[index],masterBreedName: "",profilePicURL: "",),
          ),
        );
      },
      trailing: const Icon(Icons.chevron_right),
      title: RichText(
        text: TextSpan(
            text: capitalize(suggestionList[index].name).substring(0, query.length),
            style: TextStyle(
                color: Theme.of(context).colorScheme.primary, fontWeight: FontWeight.bold),
            children: [
              TextSpan(
                  text: capitalize(suggestionList[index].name).substring(query.length),
                  style: const TextStyle(color: Colors.grey)),
            ]),
      ),
    ),
      itemCount: suggestionList.length,
    );
  }
}