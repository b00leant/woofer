/// *
/// This class is used to describe the model
/// of the photo showed in Modal and GridView
///
/// - [imageURL] the one and only attribute of this class, pretty much nonsense
/// but if the API will offer much more info about the pictures, it should be
/// great to put that payload here
class PhotoItem {
  /// TODO add eventually more data other than [imageURL]
  /// in order to enhance the modal or the GridView

  final String imageURL;

  /// [PhotoItem] constructor
  PhotoItem(this.imageURL);
}