import 'dart:convert';

import 'package:woofer/utils/helpers.dart';

///
/// Breed class describe the [Breed] model used inside for API call mapping
/// and data persistence inside SharedPreferences
///
/// - [name] is the name of the breed 💬
/// - [imageURL] is a http url pointing to an image source 📷
/// - [subBreeds] is the field containing the Breed's sub breeds 🐶.
class Breed {
  String name;
  String imageURL;
  List<Breed> subBreeds;

  ///[Breed] constructor
  Breed({
    required this.name,
    required this.imageURL,
    required this.subBreeds
  });

  /// *
  /// this helper is used for mapping purposes
  /// and allowed Flutter to convert a single Breed
  /// instance into the right JSON structure
  Map<String, dynamic> toJson() => {
    "name": name,
    "imageURL": imageURL,
    "subBreeds": jsonEncode(subBreeds),
  };

  /// *
  /// this helper is used for mapping purposes
  /// and allowed Flutter to convert a single JSON
  /// structure into the right Breed instance
  factory Breed.fromJson(Map<String, dynamic> json) => Breed(
      name: json["name"],
      imageURL: json["imageURL"],
      subBreeds: breedListFromJson(json['subBreeds']),
  );

}

