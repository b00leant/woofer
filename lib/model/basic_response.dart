/// [BasicResponse] describes the JSON HTTP body
/// of each API response from "dog.ceo",
/// - [status] is the HTTP code mapped inside the response (200,400,418..)
/// - [message] is the field used for data
class BasicResponse {
  String status;
  dynamic message;

  /// [BasicResponse] constructor defines the data structure of the JSON from any
  /// HTTP response from "dog.ceo",
  /// - [status] is the HTTP code mapped inside the response (200,400,418..)
  /// - [message] is the field used for data
  BasicResponse({required this.status, required this.message});

  /// the [fromJson] helper is used during the API call to map
  /// the String to any [BasicResponse] object instance
  factory BasicResponse.fromJson(Map<String, dynamic> json) => BasicResponse(
      status: json["status"], message: json["message"]);
}