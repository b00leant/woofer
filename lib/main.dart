import 'package:flutter/material.dart';
import 'package:theme_provider/theme_provider.dart';
import 'package:woofer/constants/routes.dart';
import 'package:woofer/constants/themes.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}
class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  /// *
  /// [saveThemesOnChange] is used to save automatically the theme onChange
  /// [loadThemeOnInit] is used to load automatically the theme onInit
  Widget build(BuildContext context) {
    return ThemeProvider(
      saveThemesOnChange: true,
      loadThemeOnInit: true,
      themes: getAppThemes(context),
      child: ThemeConsumer(
        child: Builder(
            builder: (themeContext) =>
                MaterialApp(
                  debugShowCheckedModeBanner: false,
                  title: 'Woofer',
                  theme: ThemeProvider.themeOf(themeContext).data,
                  initialRoute: '/list',
                  routes: routes
                )
        ),
      ),
    );
  }
}