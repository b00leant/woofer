import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// extends the default [StatelessWidget]
///
/// this class describe the BreedImage Widget,
/// a component that is used to render the square images inside the
/// @GridList Widget
/// - [url] is the http url to an image
class BreedGridItem extends StatelessWidget{
  const BreedGridItem({super.key, required this.url});
  final String url;

  @override
  Widget build(BuildContext context) {
    return Image.network(
      url,
      errorBuilder: (BuildContext context,
          Object exception, StackTrace? stackTrace) {
        return const Icon(
          Icons.pets,
          size: 50,
        );
      },
      fit: BoxFit.cover,
      loadingBuilder: (BuildContext context,
          Widget child,
          ImageChunkEvent? loadingProgress) {
        if (loadingProgress == null) return child;
        return Center(
          child: Icon(Icons.insert_photo_outlined,size:50,color: Theme.of(context).colorScheme.secondary,),
        );
      },
    );
  }

}