import 'package:flutter/material.dart';
import 'package:woofer/model/breed.dart';
import 'package:woofer/utils/helpers.dart';

/// extends the default [StatefulWidget]
///
/// this class describe the CustomBreedItem Widget,
/// a component that is used to render the custom item inside
/// the [Listview.separated] for the [ListRoute] Widget
/// - [name] is the name of the breed showed in the [ListTile]
/// - [subBreedList] is a [List] of Breed that represent the sub breeds
/// - [withArrow] is a [bool] attribute that defines whether an [ListTile] can have an arrow to the right
/// - [randomImageURL] is the http url to a random image
class CustomBreedItem extends StatefulWidget {

  const CustomBreedItem(
      {
        super.key,
        required this.name,
        required this.subBreedList,
        required this.withArrow,
        required this.randomImageURL,
      });

  final String randomImageURL;
  final String name;
  final bool withArrow;
  final List<Breed> subBreedList;

  @override
  State<CustomBreedItem> createState() => _CustomBreedItemState();
}

class _CustomBreedItemState extends State<CustomBreedItem> {
  String imageURL = "";
  static const double avatarSize = 40.00;

  @override
  void initState() {
    super.initState();
    imageURL = widget.randomImageURL;
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: SizedBox(
          width: avatarSize,
          height: avatarSize,
          child: ClipOval(
              child: Image.network(
                imageURL,
                errorBuilder:
                    (BuildContext context, Object exception, StackTrace? stackTrace) {
                  return Icon(Icons.pets,size: avatarSize,color:  Theme.of(context).colorScheme.primary);
                },
                fit: BoxFit.cover,
              )
          )), // TO
      title: Text(capitalize(widget.name.replaceAll("/", " "))),

      trailing:  Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          widget.subBreedList.isNotEmpty?const Icon(Icons.pets_outlined):Container(),
          widget.subBreedList.isNotEmpty?Text(" ${widget.subBreedList.length} Sub Breeds"):const Text(""),
          widget.withArrow?const Icon(Icons.chevron_right_outlined):const Text(""),
        ],
      ),
      isThreeLine: false,
    );
  }
}
