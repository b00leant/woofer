import 'package:flutter/material.dart';

class BreedProgressBar extends StatelessWidget{
  const BreedProgressBar({super.key, required this.value});
  final double value;

  @override
  Widget build(BuildContext context) {
    return Stack(
        alignment: Alignment.center,
        children: [
          LinearProgressIndicator(
              value: value,
              minHeight: 5,
              valueColor: AlwaysStoppedAnimation<Color>(
                  Theme.of(context).colorScheme.primary)),
        ]
    );
  }
  
}