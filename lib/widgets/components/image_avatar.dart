import 'package:flutter/material.dart';


/// extends the default [StatefulWidget]
///
///  * [imageURL] is used to render the image over the internet
///
class BreedImageAvatar extends StatefulWidget {
  final String imageURL;

  const BreedImageAvatar({super.key, required this.imageURL});


  @override
  State<BreedImageAvatar> createState() => BreedImageAvatarState();
}

class BreedImageAvatarState extends State<BreedImageAvatar>{

  @visibleForTesting
  late String imageURL = "";

  @override
  void initState() {
  // TODO: implement initState
  super.initState();
}

  @override
  Widget build(BuildContext context) {
    imageURL = widget.imageURL;
    return  Container(
      padding: const EdgeInsets.all(5), // Border width
      decoration: BoxDecoration(color: Theme.of(context).colorScheme.inverseSurface, shape: BoxShape.circle),
      child: ClipOval(
        child: SizedBox.fromSize(
          size: const Size.fromRadius(48), // Image radius
          child: imageURL!=""?(Image.network(imageURL, errorBuilder:
              (BuildContext context, Object exception, StackTrace? stackTrace) {
            return const Icon(Icons.pets,size: 50,);
          },fit: BoxFit.cover)):const Icon(Icons.pets,size: 50,),
        ),
      ),
    );
  }
}