import 'package:flutter/material.dart';
import 'package:woofer/services/api_services.dart';
import 'package:woofer/widgets/components/image_avatar.dart';
import 'package:http/http.dart' as http;
/// extends the default [StatefulWidget]
/// TODO CHANGE DOC
/// this class describe the UI/UX of the [DynamicBreedImageAvatar] Widget
///
/// This widget is a custom avatar for the [DetailRoute] page,
/// - [currentURL] is the last loaded profile picture url src
/// - [breedName] is the breed name to show under the avatar
/// - [masterBreedName] is the ancestorBreedName;
class DynamicBreedImageAvatar extends StatefulWidget{

  final String currentURL;
  final String breedName;
  final String masterBreedName;

  /// Getter for [breedName] attribute
  String getBreedName(){
    return breedName;
  }

  /// Getter for [masterBreedName] attribute
  String getMasterBreedName(){
    return masterBreedName;
  }

  const DynamicBreedImageAvatar({super.key, required this.breedName, required this.masterBreedName, required this.currentURL});

  @override
  State<DynamicBreedImageAvatar> createState() => DynamicBreedImageAvatarState();
}

@visibleForTesting
class DynamicBreedImageAvatarState extends State<DynamicBreedImageAvatar> {

  late http.Client client;
  late APIService apiService;
  late String currentURL;

  @override
  void initState() {
    super.initState();
    client = http.Client();
    apiService = APIService(client);
    currentURL = widget.currentURL;
    if(currentURL == ""){
      changeRandomPic();
    }
  }
  @override
  Widget build(BuildContext context) {
    return  Stack(
        clipBehavior: Clip.none,
        children: [
          Positioned(child: BreedImageAvatar(imageURL: currentURL,key: const Key('image-avatar'))),
          Positioned(left:50,top:60,child: RawMaterialButton(
              onPressed: changeRandomPic,
              elevation: 2.0,
              fillColor:
              Theme.of(context).colorScheme.primary,
              padding: const EdgeInsets.all(6.0),
              shape: const CircleBorder(),
              child: Icon(Icons.shuffle,color: Theme.of(context).colorScheme.inversePrimary))),
        ]);
  }

  getAsyncRandomPicture() async {
    String randomImage = "";

    if(widget.masterBreedName == ""){
      // if you have no master breed, go with the 'singleBreed' API
      randomImage = await apiService.getSingleRandomBreedImage( widget.breedName);
    }else{
      // if you have masterBreed, you must use another API
      randomImage = await apiService.getSingleSubBreedRandomImage( widget.masterBreedName,widget.breedName);
    }

    currentURL = randomImage;
    setState(() {
      currentURL = randomImage;
    });
  }

  changeCurrentURL(String url){
    setState(() {
      currentURL = url;
    });
  }


  changeRandomPic() {
    getAsyncRandomPicture();
  }

}