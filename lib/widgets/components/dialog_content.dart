import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:woofer/widgets/components/grid_item.dart';

class BreedDialogContent extends StatelessWidget {
  final String url;
  const BreedDialogContent({super.key, required this.url});

  @override
  Widget build(BuildContext context) {
    return Stack(alignment: Alignment.center, children: [
      Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        BreedGridItem(url: url),
      ],
    ),
    Positioned(right:10,top:5,child: TextButton(
      onPressed: () {
        Navigator.pop(context);
      },
      child: const Icon(Icons.close,size: 40,)
    )),
    ]);
  }
}