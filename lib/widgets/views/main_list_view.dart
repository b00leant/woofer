import 'dart:convert';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:woofer/model/breed.dart';
import 'package:woofer/services/api_services.dart';
import 'package:woofer/utils/helpers.dart';
import 'package:woofer/widgets/components/list_item.dart';
import 'package:woofer/widgets/components/progressbar.dart';
import 'package:woofer/widgets/views/base_layout_view.dart';
import 'detail_view.dart';
import 'package:http/http.dart' as http;

/// extends the default [StatefulWidget]
///
/// this class describe the UI/UX of the [MainListView] Widget
/// - [scrollController] is used to manage the behaviours of scrolling gestures
/// - [urls] is the List<String> which contains the src for each image showed in grid
class MainListView extends StatefulWidget {
  const MainListView({super.key});
  @override
  State<MainListView> createState() => _MainListViewState();
}

class _MainListViewState extends State<MainListView> {

  late http.Client client;
  late APIService apiService;
  bool _autoScrollEnabled = false;
  bool _loading = true;
  double _loadingValue = 0.0;
  final ScrollController _scrollController = ScrollController();

  String randomSentence = "";
  List<Breed> breedDataList = [];

  @override
  void initState() {
    super.initState();
    loadAndSaveData();
    client = http.Client();
    apiService = APIService(client);
    _scrollController.addListener(_scrollListener);
  }
  Future<void> _refreshBreed() async{
      String newRandomSentence = randomFunnySentence();
      while(newRandomSentence == randomSentence){
        newRandomSentence = randomFunnySentence();
      }
      randomSentence = newRandomSentence;
      breedDataList = [];
      setState(() {
        breedDataList = [];
      });
      resetPersistence();
      loadAndSaveData();
  }

  void _scrollListener() {
    bool isTop = _scrollController.position.pixels == 0;
    if (isTop) {
      setState(() {
        _autoScrollEnabled = false;
      });
    } else {
      setState(() {
        _autoScrollEnabled = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return RefreshIndicator(onRefresh: _refreshBreed,
    child:BaseLayoutView(
      // added a helper fab to scroll on top easies
      fab: _autoScrollEnabled?FloatingActionButton(
        child: const Icon(Icons.arrow_upward,size: 30,),
        onPressed: (){
        _scrollController.animateTo(
          0.0,
          curve: Curves.easeOut,
          duration: const Duration(milliseconds: 300),
        );
      },):null,
      title: 'Breed List',
      withSearch: !_loading,
      breedDataList: breedDataList,
      child: Stack(children: [
        _loading
            ? Opacity(
                opacity: _loading ? 0.3 : 0.0,
                child: Container(
                  color: Colors.black,
                ),
              )
            : ListView.separated(
                controller: _scrollController,
                separatorBuilder: (context, index) => const Divider(
                    height: 5, color: Colors.grey, thickness: 1.0),
                //padding: const EdgeInsets.only(top: 50.0, left: 6.0, right: 6.0),
                itemCount: breedDataList.length,
                itemBuilder: (context, index) {
                  final Breed breedItem = breedDataList[index];
                  return GestureDetector(
                      child: CustomBreedItem(
                        name: breedItem.name,
                        subBreedList: breedItem.subBreeds,
                        withArrow: true,
                        randomImageURL: breedItem.imageURL,
                      ),
                      onTap: () => {
                            Navigator.of(context).push(MaterialPageRoute(
                              builder: (_) => DetailView(
                                profilePicURL: breedItem.imageURL,
                                breedData: breedItem,
                                masterBreedName: "",
                              ),
                            ))
                          });
                },
              ),
        _loading == true
            ? Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children:[
                      Text(textAlign: TextAlign.center,randomSentence,style: Theme.of(context).textTheme.headlineSmall?.copyWith(color: Theme.of(context).colorScheme.primary )),
                  Icon(Icons.pets_sharp,size: 60,color: Theme.of(context).colorScheme.primary,),
                  const Divider(height: 20,color: Colors.transparent,thickness: 0.0,),
                  SizedBox(
                    width: screenWidth / 2,
                    child: BreedProgressBar(value: _loadingValue,))]))
            : Container(),
      ]),
    ));
  }

  /// *
  /// used to add return a single random image URL
  /// @breed
  Future<String> getSingleImageURL(String breed) async {
    return await apiService.getSingleRandomBreedImage(breed);
  }

  /// *
  /// @breedList
  ///
  /// Add to each Breed element of [breedList] a random image
  ///
  /// Does side-effect on [breedList]
  Future<List<Breed>> setBreedDataImages(List<Breed> breedList) async {
    _loadingValue = 0.0;
    for (int i = 0; i < breedList.length; i++) {
      Breed el = breedList[i];
      setState(() {
        _loadingValue = (((i * 100) / (breedList.length - 1))) / 100;
      });
      String randomURL = await apiService.getSingleRandomBreedImage(el.name);
      el.imageURL = randomURL;
      List<Breed> subBreeds = [];
      for (int j = 0; j < el.subBreeds.length; j++) {
        Breed subEl = el.subBreeds[j];
        // Hack used to pass the correct sub-breed path in order to use the same service
        randomURL = await apiService.getSingleRandomBreedImage("${el.name}/${subEl.name}");
        Breed subBreed =
            Breed(name: subEl.name, imageURL: randomURL, subBreeds: []);
        subBreed.imageURL = randomURL;
        subBreeds.add(subBreed);
      }
      el.subBreeds = subBreeds;
    }
    return breedList;
  }

  /// *
  /// this function loads the List<Breed> data list using the
  /// [getBreedList] helper
  Future<List<Breed>> getAllBreedList() async {
    setState(() {
      _loading = true;
    });

    breedDataList = await apiService.getBreedList();
    List<Breed> aux = await setBreedDataImages(breedDataList);
    breedDataList = aux;

    setState(() {
      _loading = false;
    });
    return Future.value(breedDataList);
  }

  /// *
  /// used to load the List<Breed> data list using the
  /// [SharedPreferences] features
  Future<List<Breed>> getFromPersistence() async {
    setState(() {
      _loading = true;
    });
    List<Breed> decodedList = [];
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //await prefs.remove('breedDataList')!;
    if (prefs.getString('breedDataList') != null) {
      String breedJSONList = prefs.getString('breedDataList')!;
      if (breedJSONList != "") {
        List<Breed> decodedList = breedListFromJson(breedJSONList);
        breedDataList = decodedList;
        setState(() {
          breedDataList = decodedList;
          _loading = false;
        });
      }
    }
    return Future.value(decodedList);
  }

  /// *
  /// this helper function is used to save a List<Breed> [listToSave] on persistence
  /// it returns a Future<void>
  Future<void> setToPersistence(List<Breed> listToSave) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String breedJSONList = jsonEncode(listToSave);
    await prefs.setString('breedDataList', breedJSONList);
    return Future.value();
  }

  /// *
  /// this helper function used to get the data saved
  /// on persistence SharedPreferences,
  /// the code takes the data weather is possible,
  /// otherwise it loads from HTTPs
  void loadAndSaveData() async {
    await getFromPersistence();
    if (breedDataList.isEmpty) {
      breedDataList = await getAllBreedList();
      if (breedDataList.isNotEmpty) {
        await setToPersistence(breedDataList);
      }
    }
  }

  ///
  /// this helper is used to load a random [String]
  /// sentence showed in the data loading
  String randomFunnySentence() {
    // generates a new Random object
    final random = Random();
    List<String> sentences = [
      "Woff Woff!\n ..ehm I mean 'loading!'",
      "Take a breath or two\nI'm downloading!",
      "Internet is a wonderful place\n where to pick dogs",
      "Refreshing some breeds!"
    ];
    return sentences[random.nextInt(sentences.length)];
  }

}
