import 'package:flutter/material.dart';
import 'package:woofer/model/breed.dart';
import 'package:woofer/utils/helpers.dart';
import 'package:woofer/services/api_services.dart';
import 'package:woofer/widgets/components/dynamic_image_avatar.dart';
import 'package:woofer/widgets/components/list_item.dart';
import 'package:woofer/widgets/views/grid_list_view.dart';
import 'package:http/http.dart' as http;

/// extends the default [StatefulWidget]
///
/// this class describe the UI/UX of the [DetailView] Widget
/// - [breedData] is the Breed model information's that will be shown in the Widget
/// - [profilePicURL] the last loaded profile picture
/// - [masterBreedName] if present, a breed ancestor
///
class DetailView extends StatefulWidget {

    DetailView({super.key, this.breedData, this.masterBreedName,this.profilePicURL});
  final Breed? breedData;
  String? profilePicURL;
  String? masterBreedName;

  @override
  State<DetailView> createState() => _DetailViewState();
}

class _DetailViewState extends State<DetailView> {
  bool _loading = false;
  int _selectedIndex = 0;
  late http.Client client;
  late APIService apiService;
  List<String> images = [];
  final _controller = ScrollController();

  @override
  void initState() {
    client = http.Client();
    apiService = APIService(client);
    super.initState();
    getImages();
  }

  void getImages() async {
    setState(() {
      _loading = true;
    });
    if (widget.breedData?.name != null) {

      if(widget.masterBreedName == ""){
        // if you have no master breed, go with the 'singleBreed' API
        images = await apiService.getBreedImages(widget.breedData!.name);
      }else{
        // if you have masterBreed, you must use another API
        images = await apiService.getSubBreedImages(widget.masterBreedName!,widget.breedData!.name);
      }

      /*
      *
      * NOTE: I made the decision to set a photo limit, since there is no
      * pagination and I still have to find a workaround for the huge loading
      * lag caused from the mapping that merge multiple API calls.
      */
      if (images.length >= 50) {
        images = images.sublist(0, 50);
      }
    }
    setState(() {
      _loading = false;
      images = images;
    });
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
            actions: createAppBarActions(context),
            backgroundColor: Theme.of(context).colorScheme.inversePrimary,
            title: const Text("Breed Detail") //args.breedData.name),
            ),
        body: Stack(children:[_loading
            ? Opacity(
          opacity: _loading ? 0.4 : 0.0,
          child: Container(
            color: Colors.black,
          ),
        ):Container(),
        SingleChildScrollView(
            child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
                decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.inversePrimary,
        border: Border(bottom: BorderSide(
          color:Theme.of(context).colorScheme.secondary, //                   <--- border color
          width: 3.0,
        ),
        ),
        ),
                child: Padding(
                    padding: const EdgeInsets.all(45.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children:[
                        Column(
                          children: [
                            DynamicBreedImageAvatar(breedName: widget.breedData!.name, masterBreedName: widget.masterBreedName!, currentURL: widget.profilePicURL!,),
                            const Divider(),
                            Text(
                              capitalize(widget.breedData!.name),
                              style: Theme.of(context)
                                  .textTheme
                                  .headlineSmall!
                                  .copyWith(color: Theme.of(context).colorScheme.primary),
                            ),
                          ],
                        ),
                        const Spacer(),
                        Column(
                          children: [
                            widget.masterBreedName!=""?
                            (Row(children: [
                              Row(children:[
                                const Icon(Icons.account_tree,size: 30,),
                                Text(" ${capitalize(widget.masterBreedName!)}",style: Theme.of(context)
                                    .textTheme
                                    .headlineLarge!
                                    .copyWith(
                                  color: Theme.of(context).colorScheme.primary,
                                )),
                                ]),
                            ])):Container(),
                            const Divider(),
                            Row(children: [
                              widget.breedData!.subBreeds.isNotEmpty
                                  ? const Icon(
                                      Icons.pets,
                                      //color: Colors.white,
                                    )
                                  : Container(),
                              Text(
                                  " ${widget.breedData!.subBreeds.length.toString()}",
                                  style: Theme.of(context)
                                      .textTheme
                                      .headlineLarge!
                                      .copyWith(
                                        color: Theme.of(context).colorScheme.primary//Colors.white,
                                      )),
                            ]),
                            Text("Sub Breeds",
                                style: Theme.of(context)
                                    .textTheme
                                    .headlineSmall!
                                    .copyWith(color: Theme.of(context).colorScheme.primary//Colors.white
                                )),
                          ],
                        ),
                      ],
                    ))),
            _selectedIndex == 1
                ? ListView.separated(
                    shrinkWrap: true,
                    controller: _controller,
                    separatorBuilder: (context, index) => Divider(
                        height: 5, color: Theme.of(context).colorScheme.inversePrimary, thickness: 1.0),
                    padding:
                        const EdgeInsets.only(top: 20.0, left: 6.0, right: 6.0),
                    itemCount: widget.breedData!.subBreeds.length,
                    itemBuilder: (context, index) {
                      final Breed breedItem =
                          widget.breedData!.subBreeds[index];
                      return GestureDetector(
                        onTap: () =>{
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (_) => DetailView(masterBreedName:widget.breedData!.name,breedData: breedItem,profilePicURL: breedItem.imageURL,),
                          ))
                        },
                          child: CustomBreedItem(
                        name: "${widget.breedData!.name}/${breedItem.name}",
                        subBreedList: const [],
                        withArrow: true,
                        randomImageURL: breedItem.imageURL,
                      ));
                    },
                  )
                : GridListView(urls: images, scrollController: _controller)
            //PicGridItem(breedName: args.breedData.name)
          ],
        )),
        _loading?Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children:[
                  Icon(Icons.pets_sharp,size: 60,color: Theme.of(context).colorScheme.primary,),
                  const Divider(height: 20,color: Colors.transparent,thickness: 0.0,),
                  CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(
                          Theme.of(context).colorScheme.primary))])):
        Container()]),
        bottomNavigationBar:
        widget.masterBreedName==""?
        BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.photo),
              label: 'Photos',
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.list),
                label: 'Sub Breed List',
              )
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Theme.of(context).colorScheme.primary,
          onTap: _onItemTapped,
        ):null
    );
  }

}
