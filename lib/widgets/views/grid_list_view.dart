import 'package:flutter/material.dart';
import 'package:woofer/utils/helpers.dart';
import 'package:woofer/widgets/components/dialog_content.dart';
import 'package:woofer/widgets/components/grid_item.dart';

/// extends the default [StatefulWidget]
///
/// this class describe the UI/UX of the [GridListView] component
/// - [scrollController] is used to manage the behaviours of scrolling gestures
/// - [urls] is the List<String> which contains the src for each image showed in grid
class GridListView extends StatefulWidget {
  const GridListView({super.key,required this.urls, required this.scrollController});
  final List<String> urls;
  final ScrollController scrollController;

  @override
  State<GridListView> createState() => _GridListViewState();
}

class _GridListViewState extends State<GridListView> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      shrinkWrap: true,
      controller: widget.scrollController,
      padding: const EdgeInsets.only(
          top: 3.0, left: 3.0, right: 3.0, bottom: 3.0),
      itemCount: widget.urls.length,
      itemBuilder: (context, index) {
        final String url = widget.urls[index];
        return GestureDetector(
          onLongPress: () {
            loadShareBottomMenu(context, url);
            },
          onTap: () =>{
            showDialog<String>(
              context: context,
              builder: (BuildContext context) => Dialog.fullscreen(
                backgroundColor: Colors.black,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: BreedDialogContent(url: widget.urls[index],)
                ),
              ),
            ),
          },
            child: Container(
                color: Theme.of(context).colorScheme.inverseSurface,
                child:BreedGridItem(url: url))
        );
      },
      gridDelegate:
      const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisSpacing: 3,
        mainAxisSpacing: 3,
        crossAxisCount: 3,
      ),
    );
  }
}