import 'package:flutter/material.dart';
import 'package:woofer/model/breed.dart';
import 'package:woofer/delegates/search_delegate.dart';
import 'package:woofer/services/api_services.dart';
import 'package:woofer/utils/helpers.dart';
import 'package:http/http.dart' as http;

/// extends the default [StatefulWidget]
///
/// this class describes the layout that's shared between routes that
/// want to appear in similar ways, maintaining the Material Scaffold design
/// - [title] is used to set a title
/// - [child] is the Widget field that will fill this layout
class BaseLayoutView extends StatefulWidget {

  /// *
  /// [BaseLayoutView] constructor
  /// - [title] is the Label displayed on this template's [AppBar]
  /// - [breedDataList] is the breed dataStructure, passed for casual purposes
  /// - [child] is the rendered Widget inside the layout
  /// - [withSearch] enable/disable the [showSearch] function for [SearchDelegate]
  const BaseLayoutView({
    required this.fab,
    required this.title,
    this.breedDataList,
    required this.child,
    required this.withSearch,Key? key}):super(key: key);

  final FloatingActionButton? fab;
  final Widget child;
  final List<Breed>? breedDataList;
  final bool withSearch;
  final String title;


  @override
  State<BaseLayoutView> createState() => BaseLayoutViewState();
}

class BaseLayoutViewState extends State<BaseLayoutView> {
  late APIService apiService;
  late http.Client client;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  FloatingActionButton? floatingActionButton;
  @override
  Widget build(BuildContext context) {
    List<IconButton> extraActions = [];
    if(widget.withSearch && widget.breedDataList != null){
        extraActions = [
        IconButton(icon: const Icon(Icons.search),
            onPressed: () {
              showSearch(context: context, delegate: DataSearch(widget.breedDataList!));
            })
      ];
    }
    return WillPopScope(
        //forbidden swipe in iOS(my ThemeData(platform: TargetPlatform.iOS,)
        onWillPop: () async {
          if (Navigator.of(context).userGestureInProgress) {
            return false;
          } else {
            return true;
          }
        },
        child: Scaffold(
            floatingActionButton: widget.fab,
            appBar: AppBar(
              actions: createAppBarActions(context,extraActions),
              backgroundColor: Theme.of(context).colorScheme.inversePrimary,
              title: Text(widget.title),
            ),
            body: widget.child
        )
    );
  }
}
