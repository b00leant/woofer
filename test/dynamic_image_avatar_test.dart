import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:woofer/widgets/components/dynamic_image_avatar.dart';
import 'package:woofer/widgets/components/image_avatar.dart';


void main() {

  setUpAll(() {
    // ↓ required to avoid HTTP error 400 mocked returns without Mockito
    HttpOverrides.global = null;
  });
  group('Dynamic BreedImageAvatar', ()
  {
    testWidgets('should properly change the image when pressin the button', (WidgetTester tester) async {
      Widget parentWidget = const MaterialApp(
        home: DynamicBreedImageAvatar(
            breedName: 'basset',
          masterBreedName: 'hound',
          key: Key('dynamic-image-avatar'), currentURL: 'https://images.dog.ceo/breeds/hound-walker/n02089867_3449.jpg',),
      );

      await tester.pumpWidget(parentWidget);
      //final targetWidget = find.byKey(const Key('dynamic-image-avatar'));
      DynamicBreedImageAvatar targetWidget = tester.widget(
          find.byKey(const Key('dynamic-image-avatar')));
      print("passed breedName is ${targetWidget.getBreedName()}");
      print("passed masterBreedName is ${targetWidget.getMasterBreedName()}");

      BreedImageAvatar childWidget = tester.widget(
          find.byKey(const Key('image-avatar')));

      print("old url is ${childWidget.imageURL} ");

      final button = find.byType(RawMaterialButton);
      await tester.tap(button);

      print("tap on shuffle button!");
      await tester.pumpAndSettle(
        const Duration(seconds: 8),
        EnginePhase.sendSemanticsUpdate,
        const Duration(minutes: 10),
      );
      childWidget = tester.widget(
          find.byKey(const Key('image-avatar')));

      await tester.pumpWidget(childWidget);
      print("new url is ${childWidget.imageURL}");


      final childFinder = find.descendant(
          of: find.byWidget(childWidget), matching: find.byType(Image));
      expect(childFinder, findsOneWidget);

      print("expected to find one image! ");
    });


    testWidgets('should not show the image', (WidgetTester tester) async {
      Widget parentWidget = const MaterialApp(
        home: DynamicBreedImageAvatar(breedName: 'hound',
          masterBreedName: 'bassetttt',
          key: Key('dynamic-image-avatar'), currentURL: '',),
      );

      await tester.pumpWidget(parentWidget);
      await tester.pumpAndSettle(
        const Duration(milliseconds: 1000),
        EnginePhase.build,
        const Duration(minutes: 10),
      );

      BreedImageAvatar childWidget = tester.widget(
          find.byKey(const Key('image-avatar')));

      print("passed url is ${childWidget.imageURL} (none)");

      BreedImageAvatar targetWidget = tester.widget(
          find.byKey(const Key('image-avatar')));
      final childFinder = find.descendant(
          of: find.byWidget(targetWidget), matching: find.byType(Image));
      expect(childFinder, findsNothing);
      final childFinder2 = find.descendant(
          of: find.byWidget(targetWidget), matching: find.byType(Icon));
      expect(childFinder2, findsOneWidget);
      print("expected to not find images, but only one Icons");
    });








  });
}


/*
    DynamicBreedImageAvatar targetWidget = tester.widget(find.byKey(const Key('dynamic-image-avatar')));

    final childFinder = find.descendant(of: find.byWidget(targetWidget), matching: find.byType(BreedImageAvatar));
    expect(childFinder, findsOneWidget);
    */
//final grandChild = find.descendant(of: find.byWidget(childFinder), matching: find.byType(Icon));
//expect(grandChild, findsOneWidget);

/*mockNetworkImagesFor(() => tester.runAsync(() async {
      final DynamicBreedImageAvatarState state = tester.state(find.byType(DynamicBreedImageAvatar));
      state.currentURL;
    })
    );*/

//final breedImageAvatar = find.descendant(of: find.byType(DynamicBreedImageAvatar), matching: find.byType(BreedImageAvatar));
//breedImageAvatar.
//mockNetworkImagesFor(() => tester.pumpWidget(BreedImageAvatar(imageURL: validImageURL)));
//final breedImageAvatar = find.descendant(of: find.byType(DynamicBreedImageAvatar), matching: find.byType(BreedImageAvatar));
//expect(breedImageAvatar, findsOneWidget);



/*
      BreedImageAvatar childTargetWidget = tester.widget(
          find.byKey(const Key('image-avatar')));


      await tester.pumpWidget(parentWidget);
      await tester.pump(const Duration(milliseconds: 5000));

      final childFinder = find.descendant( of: find.byWidget(childTargetWidget), matching: find.byType(Image));
      expect(childFinder, findsOneWidget);
      */

/*
      BreedImageAvatar targetWidget = tester.widget(
          find.byKey(const Key('image-avatar')));
      final childFinder = find.descendant(
          of: find.byWidget(targetWidget), matching: find.byType(Image));
      expect(childFinder, findsOneWidget);*/

