// Import the test package and Counter class
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:woofer/widgets/components/image_avatar.dart';


void main() {

  //const String validImageBase64 = "iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAFUlEQVR42mP8z8BQz0AEYBxVSF+FABJADveWkH6oAAAAAElFTkSuQmCC";

  String validImageURL = "https://fastly.picsum.photos/id/444/200/300.jpg?hmac=xTzo_bbWzDyYSD5pNCUYw552_qtHzg0tQUKn50R6FOM";
  String invalidImageURL = "https://fastly.picsum.photos/randompath";

  group('BreedImageAvatar', () {

    testWidgets(
      'should properly mock Image.network and not crash', (WidgetTester tester) async {

      Widget breedImageAvatar = BreedImageAvatar(imageURL: validImageURL,);
      await tester.pumpWidget(breedImageAvatar);
      final childFinder = find.descendant(of: find.byWidget(breedImageAvatar), matching: find.byType(Image));
      expect(childFinder, findsOneWidget);

    });

    testWidgets(
        'should properly show the Icon.pets', (WidgetTester tester) async {

      Widget breedImageAvatar = BreedImageAvatar(imageURL: invalidImageURL,);
      await tester.pumpWidget(breedImageAvatar);
      final childFinder = find.descendant(of: find.byWidget(breedImageAvatar), matching: find.byType(Image));
      expect(childFinder, findsOneWidget);

    });

    testWidgets(
        'should properly shuffle the Icon.pets', (WidgetTester tester) async {

      Widget breedImageAvatar = BreedImageAvatar(imageURL: validImageURL,);
      await tester.pumpWidget(breedImageAvatar);
      final childFinder = find.descendant(of: find.byWidget(breedImageAvatar), matching: find.byType(Image));
      expect(childFinder, findsOneWidget);

    });
  });

}